#ifndef KMA_NOTIF_H_
#define KMA_NOTIF_H_

#include "kma_account.h"
#include "kma_auth.h"
#include "kma_status.h"

#define KMA_DEFAULT_NOTIF_LIMIT 20

enum notif_type
{
    KMA_NOTIF_UNKNOWN,
    KMA_NOTIF_FOLLOW,
    KMA_NOTIF_FOLLOW_REQUEST,
    KMA_NOTIF_MENTION,
    KMA_NOTIF_REBLOG,
    KMA_NOTIF_FAVOURITE
};

typedef struct kma_notif
{
    char* id;
    enum notif_type type;
    kma_account* account;
    kma_status* status;
} kma_notif;

void
kma_destroyNotif(kma_notif* notif);

kma_notif**
kma_getNotifs(kma_conn* conn,
              // OPTIONAL ->
              char* max_id,
              char* min_id,
              int limit,
              enum notif_type* exclude_types,
              char* account_id);

kma_notif*
kma_getSingleNotif(kma_conn* conn, char* id);

void
kma_dismissNotif(kma_conn* conn);

void
kma_dismissSingleNotif(kma_conn* conn, char* id);

#endif // KMA_NOTIF_H_
