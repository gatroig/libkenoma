/// @file kma_auth.h Logging in, requestion tokens, creating connections.
#ifndef LIBKENOMA_KMA_AUTH_H_
#define LIBKENOMA_KMA_AUTH_H_

#include <stdint.h>

/// Don't force the user to login when asking for a code
#define KMA_NOT_FORCE_LOGIN 0
/// Force the user to login when asking for a code
#define KMA_FORCE_LOGIN 1

/// Specify we want to use the app token of the kma_conn
#define KMA_APP_TOKEN 1
/// Specify we want to use the user token of the kma_conn
#define KMA_USER_TOKEN 0

/// This default URI makes the code appear written in the page instead of
/// redirecting.
#define KMA_DEFAULT_REDIRECT "urn:ietf:wg:oauth:2.0:oob"

/** Represents a connection, we use this for authorization.
 *
 * The kma_conn structure represents the whole identification unit to consume
 * the Mastodon API. Both the user token and user token are stored in it.
 * libkenoma functions that require authorization will ask for it.
 *
 * To get an application token, the kma_createConn() function is used. That will
 * initialize the client secret and id, but also request an application token.
 * To get a user token, the kma_createConn() function should be called, followed
 * by a call of kma_getUserToken().
 *
 * In case a token is being regenerated from stored data, kma_recreateConn()
 * should be used.
 */
typedef struct kma_conn
{
    char* client_id;
    char* client_secret;
    /// URL of the instance, ex: https://instance.tld
    char* instance;
    /// Token for using the API without the need of a
    /// specific user. This is always retrieved automatically by
    /// the library upon requesting the client secret and token,
    /// even if the client token is requested afterwards.
    char* app_token;
    /// Token used for requests that need user-level
    /// authentication. This value can be retrieved with
    /// function kma_getUserToken().
    char* user_token;
} kma_conn;

/** Function provided for clients to recreate a kma_conn with the cached
 * information.
 *
 * @return A kma_conn with the user supplied informaation. Needs to be freed
 *         with kma_destroyConn().
 */
kma_conn*
kma_recreateConn(char* client_id,
                 char* client_secret,
                 char* instance,
                 char* app_token,
                 char* user_token);

/** Initializes a connection.
 *
 * This function initializates the process of kma_conn creation with an
 * app_token, client_id and client_secret, but without a user_token.
 * To get it, kma_getAuthURL and kma_getUserToken is needed.
 *
 * @return A kma_conn without user_token owned by the caller. Needs to be
 *         freed with kma_destroyConn().
 */
kma_conn*
kma_createConn(const char* url,
               const char* scope,
               const char* redirect_uri,
               const char* name,
               const char* website);

/** Deallocate a kma_conn.
 *
 * Does nothing if NULL is passed.
 */
void
kma_destroyConn(kma_conn* conn);

/** Get the URL to obtain a *code*.
 *
 * The URL returned by this function will have to be visited by the user in
 * order to get the code needed to get the user token.
 *
 * @return A char* owned by the caller. Needs to be freed with standard
 *         free().
 */
char*
kma_getAuthURL(kma_conn* conn,
               const char* scope,
               const char* redirect_uri,
               uint8_t force_login);

/// Get a token (internal function).
char*
kma_getToken(kma_conn* conn,
             const char* scope,
             const char* redirect_uri,
             const char* code,
             uint8_t type);
/// Get a token for a User. This is a wrapper for kma_getToken.
void
kma_getUserToken(kma_conn* conn,
                 const char* scope,
                 const char* redirect_uri,
                 const char* code);

/// Verifies that the app_token of a kma_conn is valid
int
kma_verifyApp(const kma_conn* conn);
/// Verifies that the user_token of a kma_conn is valid
int
kma_verifyUser(const kma_conn* conn);

/// Revoke a token
void
kma_revokeToken(kma_conn* conn);

#endif
