/// @file kma_account.h User profiles
#ifndef KMA_ACCOUNT_H_
#define KMA_ACCOUNT_H_

#include "kma_auth.h"

/** Structure representing a user account. Fields are exactly the same as they
 * are in the Mastodon documentation
 */
typedef struct kma_account
{
    char* id;
    char* username;
    char* acct;
    char* display_name;
    uint8_t locked;
    uint8_t bot;
    char* created_at;
    char* note;
    char* url;
    char* avatar;
    char* avatar_static;
    char* header;
    char* header_static;
    int followers_count;
    int following_count;
    int statuses_count;
    char* last_status_at;
    // int emoji_count;
    // kma_emoji** emojis;
    // int bio_field_count;
    // kma_bio_field** fields;
} kma_account;

/** Get an account from an id.
 *
 * This is getting from id, to get from from an @ search is used
 *
 * @return kma_account pointer, owned by the caller. Needs to be freed with
 *         kma_destroyAccount().
 */
kma_account*
kma_getAccount(kma_conn* conn, char* id);

/** Retrieve an account's info from a kma_conn
 *
 * @return kma_account pointer, owned by the caller. Needs to be freed with
 *         kma_destroyAccount().
 */
kma_account*
kma_getLoggedAccount(const kma_conn* conn);

/** Deallocate a kma_conn.
 *
 * Does nothing if NULL is passed.
 */
void
kma_destroyAccount(kma_account* account);

#endif // KMA_ACCOUNT_H_
