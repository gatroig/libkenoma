#ifndef KMA_MEDIA_H_
#define KMA_MEDIA_H_

#include "kma_account.h"
#include "kma_auth.h"

enum media_type
{
    KMA_MEDIA_UNKNOWN,
    KMA_MEDIA_IMAGE,
    KMA_MEDIA_VIDEO,
    KMA_MEDIA_GIFV,
    KMA_MEDIA_AUDIO
};

typedef struct kma_media
{
    char* id;
    enum media_type type;
    char* url;
    char* preview_url;
    char* remote_url;
    char* text_url;
    // kma_meta meta;
    char* description;
    char* blurhash;
} kma_media;

#endif // KMA_MEDIA_H_
