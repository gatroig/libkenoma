/** @file kma_timeline.h Groups of posts (public, federated, a user's posts, a
 * post thread...).
 */
#ifndef KMA_TIMELINE_H_
#define KMA_TIMELINE_H_

#include <stdint.h>

#include "kma_status.h"

/// Temporary, shouldn't be hardcoded.
#define KMA_DEFAULT_TL_LIMIT 20
#define KMA_PUBLIC_TL 0
#define KMA_HOME_TL 1

/** Thread of posts
 */
typedef struct kma_statusThread
{
    /// Statuses before the one passed
    kma_status** ancestors;
    /// The status being passed
    kma_status* self;
    /// The statuses answering this one
    kma_status** descendants;
} kma_statusThread;

/**
 * Retrieves a timeline as a **NULL-terminated** array of posts. Generic
 * function, use kma_getHomeTL(), kma_getFederatedTL() and kma_getLocalTL()
 * preferably.
 *
 * @return NULL-terminated kma_status pointer array, owned by caller. Should be
 *         freed with kma_destroyTL()
 */
kma_status**
kma_getTL(const kma_conn* conn,
          uint8_t local,
          uint8_t remote,
          uint8_t only_media,
          const char* max_id,
          const char* since_id,
          const char* min_id,
          int limit,
          uint8_t type);

/** Wrapper for kma_getTL that retrieves fedeated.
 *
 * @return NULL-terminated kma_status pointer array, owned by caller. Should be
 *         freed with kma_destroyTL()
 */
kma_status**
kma_getFederatedTL(const kma_conn* conn,
                   const char* max_id,
                   const char* since_id,
                   const char* min_id,
                   int limit);

/** Wrapper for kma_getTL that retrieves the local TL.
 *
 * @return NULL-terminated kma_status pointer array, owned by caller. Should be
 *         freed with kma_destroyTL()
 */
kma_status**
kma_getLocalTL(const kma_conn* conn,
               const char* max_id,
               const char* since_id,
               const char* min_id,
               int limit);

/** Wrapper for kma_getTL that retrieves the home TL.
 *
 * @param conn Must have a valid user_token, since it is the user's timeline.
 *
 * @return NULL-terminated kma_status pointer array, owned by caller. Should be
 *         freed with kma_destroyTL()
 */
kma_status**
kma_getHomeTL(const kma_conn* conn,
              const char* max_id,
              const char* since_id,
              const char* min_id,
              int limit);

/** Deallocate a kma_conn NULL-terminated array.
 *
 * Does nothing if NULL is passed.
 */
void
kma_destroyTL(kma_status** statuses);

kma_statusThread*
kma_getStatusThread(const kma_conn* conn, const char* id);

void
kma_destroyStatusThread(kma_statusThread* thread);

#endif // KMA_TIMELINE_H_
