/** @file kma_status.h All about posts
 */
#ifndef __KMA_STATUS__
#define __KMA_STATUS__

#include "kma_account.h"
#include "kma_auth.h"
#include "kma_media.h"

typedef struct _kma_status kma_status;

struct _kma_status
{
    char* id;
    char* created_at;
    char* in_reply_to_id;
    // char *in_reply_to_account_id;
    uint8_t sensitive;
    char* spoiler_text;
    char* visibility;
    char* url;
    int replies_count;
    int reblogs_count;
    int favourites_count;
    uint8_t favourited;
    uint8_t reblogged;
    uint8_t muted;
    kma_media** media_attachments;
    // uint8_t *bookmarked;
    char* content;
    kma_status* reblog;
    char* app_name;
    kma_account* account;
    // poll;
};

#define KMA_STATUS_VISIBILITY_PUBLIC "public"
#define KMA_STATUS_VISIBILITY_UNLISTED "unlisted"
#define KMA_STATUS_VISIBILITY_PRIVATE "private"
#define KMA_STATUS_VISIBILITY_DIRECT "direct"

/* Retrieves a specific status using an ID.
 */
kma_status*
kma_getStatus(const kma_conn* conn, const char* id);

/** Deallocate a kma_status.
 *
 * Does nothing if NULL is passed.
 */
void
kma_destroyStatus(kma_status* status);

/** Publish a status
 *
 * @return Returns the post, if needed for the client
 */
kma_status*
kma_postStatus(const kma_conn* conn,
               const char* text,
               const char* replied_id,
               uint8_t sensitive,
               const char* spoiler_text,
               const char* vis);

/**
 * Fav/reblog specific status.
 *
 * Optionally a kma_status** can be passed to retrieve the post favved.
 * Returns KMA_ERROR on error. Additionally, if kma_status wasn't NULL, its
 * contents will be left untouched on error.
 */
#define KMA_STATUS_ACTION_REBLOG "reblog"
#define KMA_STATUS_ACTION_UNREBLOG "unreblog"
#define KMA_STATUS_ACTION_FAV "favourite"
#define KMA_STATUS_ACTION_UNFAV "unfavourite"
#define KMA_STATUS_ACTION_BOOKMARK "bookmark"
#define KMA_STATUS_ACTION_UNBOOKMARK "unbookmark"

/** Permorm a binary action on a status.
 */
int
kma_statusAction(const kma_conn* conn,
                 const char* id,
                 kma_status** status,
                 const char* action);

/** Turn the post's body into unformatted text.
 *
 * Useful for console or simpler applications. This function mutates the content
 * of the kma_status
 */
void
kma_statusFormat(kma_status* status);

#endif
