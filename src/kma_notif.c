#include "../include/kma_notif.h"

#include <json-c/json.h>

#include "account.h"
#include "request.h"
#include "status.h"
#include "utils.h"

static enum notif_type
notif_type_from_string(char* key)
{
    if (key == NULL)
        return KMA_NOTIF_UNKNOWN;

    struct type_key
    {
        char* string;
        enum notif_type type;
    };

    struct type_key hashmap[5] = { { "follow", KMA_NOTIF_FOLLOW },
                                   { "follow_request",
                                     KMA_NOTIF_FOLLOW_REQUEST },
                                   { "mention", KMA_NOTIF_MENTION },
                                   { "reblog", KMA_NOTIF_REBLOG },
                                   { "favourite", KMA_NOTIF_FAVOURITE } };

    int size = (int)sizeof(hashmap);
    for (int i = 0; i < size; i++) {
        if (strcmp(key, hashmap[i].string) == 0) {
            return hashmap[i].type;
        }
    }
    return KMA_NOTIF_UNKNOWN;
}

static char*
notif_string_from_enum(enum notif_type type)
{
    switch (type) {
        case KMA_NOTIF_FOLLOW:
            return "follow";
            break;
        case KMA_NOTIF_FOLLOW_REQUEST:
            return "follow_request";
            break;
        case KMA_NOTIF_MENTION:
            return "mention";
            break;
        case KMA_NOTIF_REBLOG:
            return "reblog";
            break;
        case KMA_NOTIF_FAVOURITE:
            return "favourite";
            break;
        default:
            return NULL;
            break;
    }
}

static kma_notif*
notif_from_json(struct json_object* json)
{

    kma_notif* notif = malloc(sizeof(kma_notif));

    if (from_json_handle_errors(notif, json) != KMA_OK) {
        return NULL;
    }

    struct json_object* acc_json = NULL;
    if (json_object_object_get_ex(json, "account", &acc_json)) {
        notif->account = account_from_json(acc_json);
    }

    struct json_object* status_json = NULL;
    if (json_object_object_get_ex(json, "status", &status_json)) {
        notif->status = status_from_json(status_json);
    }

    char* type_string = json_get_string(json, "type");

    if (type_string != NULL)
        notif->type = notif_type_from_string(type_string);
    else
        notif->type = KMA_NOTIF_UNKNOWN;

    return notif;
}

void
kma_destroyNotif(kma_notif* notif)
{
    if (notif) {
        if (notif->id)
            free(notif->id);
        if (notif->account)
            free(notif->account);
        if (notif->status)
            free(notif->status);
        free(notif);
    }
}

kma_notif*
kma_getSingleNotif(kma_conn* conn, char* id)
{
    if (!conn || !conn->user_token) {
        handle_error(
          "Passed NULL kma_conn pointer to kma_getStatus", KMA_ERR_NULL, 0);
        return NULL;
    }

    char* url = NULL;
    asprintf(&url, "%s/api/v1/notifications/%s", conn->instance, id);

    char* ret_json = request_get_auth(url, conn->user_token);
    if (!ret_json)
        return NULL;

    free(url);

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    char* json_id = json_get_string(parsed_json, "id");
    if (!json_id)
        return NULL;

    kma_notif* notif = notif_from_json(parsed_json);
    json_object_put(parsed_json);

    return notif;
}

kma_notif**
kma_getNotifs(kma_conn* conn,
              // OPTIONAL ->
              char* max_id,
              char* min_id,
              int limit,
              enum notif_type* exclude_types,
              char* account_id)
{
    if (!conn || !conn->user_token) {
        handle_error(
          "Passed NULL kma_conn pointer to kma_getNotifs", KMA_ERR_NULL, 0);
        return NULL;
    }

    char* url = NULL;
    asprintf(&url, "%s/api/v1/notifications", conn->instance);

    uint8_t first = 1;

    // TODO type
    if (limit > 0) {
        asprintf(&url, "%s%climit=%d", url, first ? '?' : '&', limit);
    }
    if (account_id) {
        asprintf(&url, "%s%caccount_id=%s", url, first ? '?' : '&', account_id);
    }
    if (max_id) {
        asprintf(&url, "%s%cmax_id=%s", url, first ? '?' : '&', max_id);
    }
    if (min_id) {
        asprintf(&url, "%s%cmin_id=%s", url, first ? '?' : '&', min_id);
    }
    if (exclude_types != NULL) {
        for (int i = 0; exclude_types[i] >= 0; i++) {
            asprintf(&url,
                     "%s%cexclude_types[%d]=%s",
                     url,
                     first ? '?' : '&',
                     i,
                     notif_string_from_enum(exclude_types[i]));
        }
    }

    char* ret_json = request_get_auth(url, conn->user_token);
    if (!ret_json)
        return NULL;

    if (!ret_json)
        return NULL;
    struct json_object* parsed_json = json_tokener_parse(ret_json);
    free(ret_json);

    // Number of notifications recieved (May be different from limit)
    int notif_count = json_object_array_length(parsed_json);

    kma_notif** notif_array = NULL;
    // +1 for delimiting NULL position
    notif_array = malloc((notif_count + 1) * sizeof(kma_notif*));
    if (!notif_array) {
        handle_error("Couldn't alloc space for notif array", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    for (int i = 0; i < notif_count; i++) {
        notif_array[i] =
          notif_from_json(json_object_array_get_idx(parsed_json, i));
    }
    // End of array
    notif_array[notif_count] = NULL;
    json_object_put(parsed_json);

    return notif_array;
}
