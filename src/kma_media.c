#include "../include/kma_media.h"

#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>

#include "request.h"
#include "utils.h"

enum media_type
type_from_string(char* key)
{
    if (key == NULL)
        return KMA_MEDIA_UNKNOWN;

    struct type_key
    {
        char* string;
        enum media_type type;
    };

    struct type_key hashmap[] = { { "image", KMA_MEDIA_IMAGE },
                                  { "video", KMA_MEDIA_VIDEO },
                                  { "gifv", KMA_MEDIA_GIFV },
                                  { "audio", KMA_MEDIA_AUDIO } };

    int size = (int)sizeof(hashmap);
    for (int i = 0; i < size; i++) {
        if (strcmp(key, hashmap[i].string) == 0) {
            return hashmap[i].type;
        }
    }
    return KMA_MEDIA_UNKNOWN;
}

kma_media*
media_from_json(struct json_object* json)
{
    kma_media* media = malloc(sizeof(kma_media));

    if (from_json_handle_errors(media, json) != KMA_OK) {
        return NULL;
    }

    media->id = json_get_string(json, "id");
    media->url = json_get_string(json, "url");
    media->preview_url = json_get_string(json, "preview_url");
    media->remote_url = json_get_string(json, "remote_url");
    media->text_url = json_get_string(json, "text_url");
    media->description = json_get_string(json, "description");
    media->blurhash = json_get_string(json, "blurhash");

    media->type = type_from_string(json_get_string(json, "type"));

    return media;
}

kma_media**
media_array_from_json(struct json_object* json)
{
    int media_count = json_object_array_length(json);

    kma_media** media_array = NULL;
    // +1 for delimiting NULL position
    media_array = malloc((media_count + 1) * sizeof(kma_media*));
    if (!media_array) {
        handle_error(
          "Couldn't alloc space for status' media array", KMA_ERR_MEMORY, 0);
        return NULL;
    }
    // Populate array
    for (int i = 0; i < media_count; i++) {
        media_array[i] = media_from_json(json_object_array_get_idx(json, i));
    }
    media_array[media_count] = NULL;

    return media_array;
}
