#ifndef __LIBKENOMA_REQUEST_H__
#define __LIBKENOMA_REQUEST_H__

#include <curl/curl.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define KMA_REQ_POST 0
#define KMA_REQ_GET 1

typedef struct kma_headers
{
    int count;
    char** headers;
} kma_headers;

/** Perform a request
 * @return char* with the string response. Owned by the caller.
 */
char*
request(const char* url, const char* fields, const char* auth, uint8_t method);
char*
request_post_auth(const char* url, const char* fields, const char* auth);
char*
request_get_auth(const char* url, const char* auth);
char*
request_post(const char* url, const char* fields);
char*
request_get(const char* url);

#endif
