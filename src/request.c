#include "request.h"
#include "utils.h"

// Functions to handle the http requests for data
// Libkenoma
//
// The char* returned by these functions are dynamically allocated.
// You need to call free yourself when needed.

// Struct to help the assembly of the returned data
// as it may be retuned in chunks
struct callback_state
{
    int prev_size;
    char* ret_string;
};

// Callback function to write data
static size_t
write_data(void* contents, size_t size, size_t nmemb, void* userp)
{
    // Cast pointer to the passed struct
    struct callback_state* state = userp;

    // If this is true, it's the first iteration
    if (!state->ret_string) {
        // +1 to accomodate space for the '\0'
        state->ret_string = malloc(nmemb * (size + 1));
        if (!state->ret_string)
            return 1;
    }
    // Subsequent calls to this callback
    else {
        // We're calling realloc with the size it had in the previous
        // iteration+the size curl is telling us that this chunk has
        state->ret_string =
          realloc(state->ret_string, state->prev_size + (nmemb * (size + 1)));
    }

    // Copy the actual data in the proper offset (prev_size is 0 at first
    // iteration, so it is safe to put it here)
    memcpy(state->ret_string + state->prev_size, contents, nmemb * size);
    // After every iteration put '\0' at the end, as we don't know if this
    // is our last time on this green earth
    state->ret_string[state->prev_size + (size * nmemb)] = '\0';
    // Update the size so the next iteration knows where we left off
    state->prev_size += size * nmemb;

    return size * nmemb;
}

char*
request(const char* url, const char* fields, const char* auth, uint8_t method)
{
    CURL* curl;
    CURLcode res = CURLE_GOT_NOTHING;

    // Initialize curl
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();

    // Returned string, response to request
    struct callback_state callback_ret = { 0, NULL };

    // If succesful
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        switch (method) {
            case KMA_REQ_POST:
                if (fields) {
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, fields);
                } else {
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
                }
                break;
            case KMA_REQ_GET:
                curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
                break;
            default:
                fprintf(stderr, "Invalid method type called");
        }

        struct curl_slist* list = NULL;

        if (auth) {

            char* auth_aux = NULL;
            asprintf(&auth_aux, "Authorization: Bearer %s", auth);
            list = curl_slist_append(list, auth_aux);

            // Corresponding options
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
            free(auth_aux);
        }

        // Assign callback function and where to write the return data
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &callback_ret);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            fprintf(stderr,
                    "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        }

        curl_slist_free_all(list);

        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    if (res != CURLE_OK)
        return NULL;

    return callback_ret.ret_string;
}

char*
request_post_auth(const char* url, const char* fields, const char* auth)
{
    return request(url, fields, auth, KMA_REQ_POST);
}

char*
request_get_auth(const char* url, const char* auth)
{
    return request(url, NULL, auth, KMA_REQ_GET);
}

char*
request_post(const char* url, const char* fields)
{
    return request(url, fields, NULL, KMA_REQ_POST);
}

char*
request_get(const char* url)
{
    return request(url, NULL, NULL, KMA_REQ_GET);
}
