#ifndef MEDIA_H_
#define MEDIA_H_

#include "../include/kma_media.h"

#include <json-c/json.h>

enum media_type
type_from_string(char* key);
kma_media*
media_from_json(struct json_object* json);
kma_media**
media_array_from_json(struct json_object* json);

#endif // MEDIA_H_
