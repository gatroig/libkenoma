#include "../include/kma_status.h"

#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>

#include "account.h"
#include "media.h"
#include "request.h"
#include "status.h"
#include "utils.h"

kma_status*
kma_getStatus(const kma_conn* conn, const char* id)
{
    if (!conn) {
        handle_error(
          "Passed NULL kma_conn pointer to kma_getStatus", KMA_ERR_NULL, 0);
        return NULL;
    }

    char* url = NULL;
    asprintf(&url, "%s/api/v1/statuses/%s", conn->instance, id);

    char* ret_json = request_get_auth(url, conn->user_token ?: conn->app_token);
    if (!ret_json)
        return NULL;

    free(url);

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    char* json_id = json_get_string(parsed_json, "id");
    if (!json_id)
        return NULL;

    kma_status* status = status_from_json(parsed_json);
    json_object_put(parsed_json);

    return status;
}

kma_status*
status_from_json(struct json_object* json)
{
    kma_status* status = malloc(sizeof(kma_status));

    if (from_json_handle_errors(status, json) != KMA_OK) {
        return NULL;
    }

    status->id = json_get_string(json, "id");
    status->created_at = json_get_string(json, "created_at");
    status->in_reply_to_id = json_get_string(json, "in_reply_to_id");
    status->sensitive = json_get_int(json, "sensitive");
    status->spoiler_text = json_get_string(json, "spoiler_text");
    status->visibility = json_get_string(json, "visibility");
    status->url = json_get_string(json, "url");
    status->replies_count = json_get_int(json, "replies_count");
    status->reblogs_count = json_get_int(json, "reblogs_count");
    status->favourites_count = json_get_int(json, "favourites_count");
    status->favourited = json_get_int(json, "favourited");
    status->reblogged = json_get_int(json, "reblogged");
    status->muted = json_get_int(json, "muted");
    status->content = json_get_string(json, "content");

    // NOTE: To check if a status is a reblog, check if this field is NULL
    status->reblog = NULL;
    struct json_object* reblog_json = NULL;
    if (json_object_object_get_ex(json, "reblog", &reblog_json) &&
        reblog_json != NULL) {
        status->reblog = status_from_json(reblog_json);
    }

    status->account = NULL;
    struct json_object* acc_json = NULL;
    if (json_object_object_get_ex(json, "account", &acc_json) &&
        acc_json != NULL) {
        status->account = account_from_json(acc_json);
    }
    status->media_attachments =
      media_array_from_json(json_get_object(json, "media_attachments"));

    // TODO: app_name lives inside "application" -> "name", get object before
    // name
    status->app_name = NULL;

    return status;
}

void
kma_destroyStatus(kma_status* status)
{
    if (status) {
        if (status->id)
            free(status->id);
        if (status->created_at)
            free(status->created_at);
        if (status->in_reply_to_id)
            free(status->in_reply_to_id);
        if (status->spoiler_text)
            free(status->spoiler_text);
        if (status->visibility)
            free(status->visibility);
        if (status->url)
            free(status->url);
        if (status->content)
            free(status->content);
        if (status->reblog)
            kma_destroyStatus(status->reblog);
        if (status->account)
            kma_destroyAccount(status->account);
        // TODO destroy media
        free(status);
    }
}

kma_status*
kma_postStatus(const kma_conn* conn,
               const char* text,
               const char* replied_id,
               uint8_t sensitive,
               const char* spoiler_text,
               const char* visibility)
{
    if (!conn) {
        handle_error(
          "Passed NULL kma_conn pointer to kma_poststatus", KMA_ERR_NULL, 0);
        return NULL;
    }

    if (!conn || !text)
        return NULL;

    char* url = NULL;
    asprintf(&url, "%s/api/v1/statuses", conn->instance);

    char* encoded_text = encode(text);
    char* fields = NULL;
    asprintf(&fields, "status=%s&", encoded_text);

    if (replied_id) {
        char* key = "in_reply_to_id=";
        fields =
          realloc(fields,
                  (strlen(fields) + strlen(key) + strlen(replied_id) + 1) *
                    sizeof(char));
        strcat(fields, key);
        strcat(fields, replied_id);
    }

    if (sensitive) {
        char* key = "&sensitive=1";
        fields =
          realloc(fields, (strlen(fields) + strlen(key) + 1) * sizeof(char));
        strcat(fields, key);
    }

    if (spoiler_text) {
        char* key = "&spoiler_text=";
        fields =
          realloc(fields,
                  (strlen(fields) + strlen(key) + strlen(spoiler_text) + 1) *
                    sizeof(char));
        strcat(fields, key);
        strcat(fields, spoiler_text);
    }

    if (visibility) {
        char* key = "&visibility=";
        fields =
          realloc(fields,
                  (strlen(fields) + strlen(key) + strlen(visibility) + 1) *
                    sizeof(char));
        strcat(fields, key);
        strcat(fields, visibility);
    }

    char* ret_json = request_post_auth(url, fields, conn->user_token);

    free_encoded(encoded_text);
    free(fields);
    free(url);

    if (!ret_json) {
        handle_error("Error when posting status", KMA_ERR_REQ, 0);
        return NULL;
    }

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    free(ret_json);
    kma_status* ret_status = status_from_json(parsed_json);

    json_object_put(parsed_json);

    return ret_status;
}

int
kma_statusAction(const kma_conn* conn,
                 const char* id,
                 kma_status** status,
                 const char* action)
{
    char* url = NULL;
    asprintf(&url, "%s/api/v1/statuses/%s/%s", conn->instance, id, action);
    if (url == NULL) {
        handle_error("Failed to assmeble URL perform action on the post.",
                     KMA_ERR_MEMORY,
                     0);
        return KMA_ERROR;
    }

    char* ret_json = request_post_auth(url, NULL, conn->user_token);
    free(url);
    if (!ret_json) {
        handle_error(
          "json error when perorming action to status", KMA_ERR_REQ, 0);
        return KMA_ERROR;
    }

    // TODO: Test error properlier, check on the JSON return for specific errors
    //       (401 unaurhorized, 404 not found, etc.)
    if (status != NULL) {
        // TODO: Test if this actually works and is handled properly
        struct json_object* parsed_json = json_tokener_parse(ret_json);
        *status = status_from_json(parsed_json);
        json_object_put(parsed_json);
    }
    free(ret_json);

    return KMA_OK;
}

// Unescape status content and format in plain text
void
kma_statusFormat(kma_status* status)
{
    if (!status) {
        handle_error("NULL status", KMA_ERR_NULL, 0);
        return;
    }

    if (!status->content) {
        handle_error("NULL content in status", KMA_ERR_NULL, 0);
        return;
    }

    int i = 0; // read head
    int j = 0; // write head)
    char insert_char;
    uint8_t is_newline;

    while (status->content[i] != '\0') {
        switch (status->content[i]) {
            // Tag state
            case '<':
                is_newline = (strncmp(&status->content[i + 1], "br", 2) == 0) ||
                             (strncmp(&status->content[i + 1], "/p", 2) == 0);

                if (is_newline) {
                    status->content[j] = '\n';
                    j++;
                }

                for (; status->content[i] != '>' && status->content[i] != '\0';
                     i++) {
                    // Keep Links
                    /* if ((strncmp(&status->content[i+1], "a", 1) == 0)) { */
                    /*     for (; status->content[i] != '>' &&
                     * status->content[i] != '\0'; i++) { */
                    /*         if ((strncmp(&status->content[i+1], "href=\"", 6)
                     * == 0)) { */
                    /*             i = i + 7; */
                    /*             for (; status->content[i] != '\"' &&
                     * status->content[i] != '>' && status->content[i] != '\0';
                     * i++) { */
                    /*                 status->content[j++] =
                     * status->content[i]; */
                    /*             } */
                    /*             break; */
                    /*         } */
                    /*         break; */
                    /*     } */
                    /* } */
                }
                break;
            // Character state
            case '&':
                insert_char = 'n';
                if (strncmp(&status->content[i + 1], "quot", 2) == 0)
                    insert_char = '\"';
                if (strncmp(&status->content[i + 1], "lt", 2) == 0)
                    insert_char = '<';
                if (strncmp(&status->content[i + 1], "gt", 2) == 0)
                    insert_char = '>';
                if (strncmp(&status->content[i + 1], "#39", 2) == 0)
                    insert_char = '\'';

                if (insert_char != 'n') {
                    status->content[j] = insert_char;
                    j++;

                    // Only eliminate characters if it's an HTML escape code
                    for (; status->content[i] != ';' &&
                           status->content[i] != '\0';
                         i++)
                        ;
                }

                break;
        }
        if (status->content[i] != '>' && status->content[i] != ';') {
            status->content[j] = status->content[i];
            j++;
        }
        i++;
    }
    status->content[j] = '\0';
}
