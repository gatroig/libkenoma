#ifndef AUTH_H_
#define AUTH_H_

#include "../include/kma_account.h"
#include "../include/kma_auth.h"

/**
 * Checks if token is valid and optionally returns logged user information.
 * If account pointer is null or type is not KMA_USER_TOKEN, nothing is
 * returned.
 */
int
verify_token(const kma_conn* conn, uint8_t type, kma_account** account);

#endif // AUTH_H_
