//    The MIT License (MIT)
//
//    Copyright (c) 2012-2020 clib authors
//
//    Permission is hereby granted, free of charge, to any person obtaining a
//    copy of this software and associated documentation files (the "Software"),
//    to deal in the Software without restriction, including without limitation
//    the rights to use, copy, modify, merge, publish, distribute, sublicense,
//    and/or sell copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//    DEALINGS IN THE SOFTWARE.

/**
 * 'asprintf.c' - asprintf
 *
 * copyright (c) 2014 joseph werle <joseph.werle@gmail.com>
 */

#include "utils.h"

int
vasprintf(char** str, const char* fmt, va_list args)
{
    int size = 0;
    va_list tmpa;

    // copy
    va_copy(tmpa, args);

    // apply variadic arguments to
    // sprintf with format to get size
    size = vsnprintf(NULL, 0, fmt, tmpa);

    // toss args
    va_end(tmpa);

    // return -1 to be compliant if
    // size is less than 0
    if (size < 0) {
        return -1;
    }

    // alloc with size plus 1 for `\0'
    *str = (char*)malloc(size + 1);

    // return -1 to be compliant
    // if pointer is `NULL'
    if (NULL == *str) {
        return -1;
    }

    // format string with original
    // variadic arguments and set new size
    size = vsprintf(*str, fmt, args);
    return size;
}

int
asprintf(char** str, const char* fmt, ...)
{
    int size = 0;
    va_list args;

    // init variadic argumens
    va_start(args, fmt);

    // format and get size
    size = vasprintf(str, fmt, args);

    // toss args
    va_end(args);

    return size;
}

// ----- End of copyright by clib authors -----

// Return value will have to be free'd
char*
json_get_string(struct json_object* json, char* string)
{
    struct json_object* object = NULL;

    if (json_object_object_get_ex(json, string, &object)) {
        if (json_object_get_string(object) != NULL) {
            return strdup(json_object_get_string(object));
        }
    }

    return NULL;
}

int
json_get_int(struct json_object* json, char* string)
{
    struct json_object* object = NULL;

    if (json_object_object_get_ex(json, string, &object)) {
        return json_object_get_int(object);
    }

    return 0;
}

struct json_object*
json_get_object(struct json_object* json, char* string)
{
    struct json_object* object = NULL;

    if (json_object_object_get_ex(json, string, &object)) {
        return object;
    }

    return NULL;
}

// Text Encoding

char*
encode(const char* text)
{
    CURL* curl = curl_easy_init();
    char* escaped_text = NULL;
    if (curl) {
        escaped_text = curl_easy_escape(curl, text, 0);
    }

    curl_easy_cleanup(curl);
    return escaped_text;
}

char*
unencode(const char* text)
{
    CURL* curl = curl_easy_init();
    char* unescaped_text = NULL;
    if (curl) {
        unescaped_text = curl_easy_escape(curl, text, 0);
        return curl_easy_unescape(curl, text, 0, NULL);
    }

    curl_easy_cleanup(curl);
    return unescaped_text;
}

void
free_encoded(char* text)
{
    // Safe to curl_free(NULL). Nothing happens.
    curl_free(text);
}

// Error handling

void
handle_error(char* text, char* type, uint8_t exit)
{
    fprintf(
      stderr, "[\e[0;31m%s\e[0m] of type %s: %s\n", KMA_ERR_PREFIX, type, text);

    if (exit) {
        fprintf(stderr, "%s: Exiting...\n", KMA_ERR_PREFIX);
        // exit(EXIT_FAILURE);
    }
}

uint8_t
from_json_handle_errors(void* pointer, struct json_object* json)
{
    if (pointer == NULL) {
        handle_error("Couldn't allocate space for structure in from_json",
                     KMA_ERR_MEMORY,
                     0);
        return KMA_ERROR;
    }
    if (!json) {
        handle_error("NULL JSON when getting structure", KMA_ERR_NULL, 0);
        free(pointer);
        return KMA_ERROR;
    }

    char* err_str = json_get_string(json, "error");
    if (err_str != NULL) {
        free(pointer);
        handle_error("json returned error:", KMA_ERR_REQ, 0);
        fprintf(stderr, "%s\n\n", err_str);
        free(err_str);
        return KMA_ERROR;
    }

    return KMA_OK;
}
