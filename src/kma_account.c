#include "../include/kma_account.h"

#include <stdlib.h>

#include "account.h"
#include "auth.h"
#include "utils.h"

kma_account*
account_from_json(struct json_object* json)
{
    kma_account* ret_acc = malloc(sizeof(kma_account));

    if (from_json_handle_errors(ret_acc, json) != KMA_OK) {
        return NULL;
    }

    ret_acc->id = json_get_string(json, "id");
    ret_acc->username = json_get_string(json, "username");
    ret_acc->acct = json_get_string(json, "acct");
    ret_acc->display_name = json_get_string(json, "display_name");
    ret_acc->created_at = json_get_string(json, "created_at");
    ret_acc->note = json_get_string(json, "note");
    ret_acc->url = json_get_string(json, "url");
    ret_acc->avatar = json_get_string(json, "avatar");
    ret_acc->avatar_static = json_get_string(json, "avatar_static");
    ret_acc->header = json_get_string(json, "header");
    ret_acc->header_static = json_get_string(json, "header_static");
    ret_acc->last_status_at = json_get_string(json, "last_status_at");

    ret_acc->locked = json_get_int(json, "locked");
    ret_acc->bot = json_get_int(json, "bot");
    ret_acc->followers_count = json_get_int(json, "followers_count");
    ret_acc->following_count = json_get_int(json, "following_count");
    ret_acc->statuses_count = json_get_int(json, "statuses_count");

    return ret_acc;
}

kma_account*
kma_getLoggedAccount(const kma_conn* conn)
{
    kma_account* account = NULL;
    verify_token(conn, KMA_USER_TOKEN, &account);
    // No need to check since it will be null anyway
    return account;
}

void
kma_destroyAccount(kma_account* account)
{
    if (account) {
        if (account->id)
            free(account->id);
        if (account->username)
            free(account->username);
        if (account->acct)
            free(account->acct);
        if (account->display_name)
            free(account->display_name);
        if (account->created_at)
            free(account->created_at);
        if (account->note)
            free(account->note);
        if (account->url)
            free(account->url);
        if (account->avatar)
            free(account->avatar);
        if (account->avatar_static)
            free(account->avatar_static);
        if (account->header)
            free(account->header);
        if (account->header_static)
            free(account->header_static);
        if (account->last_status_at)
            free(account->last_status_at);
        free(account);
    }
}
