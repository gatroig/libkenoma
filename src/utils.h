#ifndef KMA_UTILS_H
#define KMA_UTILS_H

#include <curl/curl.h>
#include <json-c/json.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KMA_ERR_PREFIX "LIBKENOMA_ERR"

#define KMA_ERR_MEMORY "MEMORY_ERR"
#define KMA_ERR_NULL "NULL_VALUE_ERR"
#define KMA_ERR_REQ "REQUEST_ERR"
#define KMA_ERR_CRITICAL "CRITICAL_ERR"
#define KMA_ERR_OTHER "CRITICAL_OTHER"

#define KMA_ERROR 0
#define KMA_OK 1

int
vasprintf(char** str, const char* fmt, va_list args);
int
asprintf(char** str, const char* fmt, ...);

char*
json_get_string(struct json_object* json, char* string);
int
json_get_int(struct json_object* json, char* string);
struct json_object*
json_get_object(struct json_object* json, char* string);

char*
encode(const char* text);
char*
unencode(const char* text);
// curl's escape and unescape functions use custom mallocs
void
free_encoded(char* text);

// Content of message, error type, to exit or not
void
handle_error(char* text, char* type, uint8_t exit);

uint8_t
from_json_handle_errors(void* pointer, struct json_object* json);

#endif
