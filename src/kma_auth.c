#include "../include/kma_auth.h"

#include <curl/curl.h>
#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>

#include "account.h"
#include "request.h"
#include "utils.h"

kma_conn*
kma_createConn(const char* url,
               const char* scope,
               const char* redirect_uri,
               const char* name,
               const char* website)
{
    // TODO: Should we provide a default website/name?
    if (url == NULL || scope == NULL || redirect_uri == NULL || name == NULL ||
        website == NULL) {
        handle_error(
          "Passed a null value, couldn't create connection", KMA_ERR_NULL, 0);
        return NULL;
    }

    kma_conn* conn = malloc(sizeof(kma_conn));
    if (conn == NULL) {
        handle_error("Couldn't alloc kma_conn.", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    // Create request URL
    char* connreq_url = NULL;
    asprintf(&connreq_url, "%s%s", url, "/api/v1/apps");
    if (connreq_url == NULL) {
        handle_error("Couldn't alloc URL for conn creation request.", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    // Assemble POST body
    char* fields = NULL;
    asprintf(&fields,
             "client_name=%s&redirect_uris=%s&scopes=%s&website=%s",
             name,
             redirect_uri,
             scope,
             website);
    if (fields == NULL) {
        handle_error("Couldn't alloc POST parameters for conn creation request.", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    // TODO: Return NULL on returncode not being 200.
    //       Reason: requesting example.com is a valid request but doesn't even
    //       return a JSON, it should return NULL
    char* ret_json = request_post(connreq_url, fields);
    free(connreq_url);
    free(fields);
    if (!ret_json) {
        handle_error(
          "Couldn't request application, returning NULL. Invalid domain?",
          KMA_ERR_OTHER,
          0);
        return NULL;
    }

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    free(ret_json);

    char* error = json_get_string(parsed_json, "error");
    if (error) {
        json_object_put(parsed_json);
        fprintf(stderr, "LIBKENOMA_ERR: API returned error: \"%s\"\n", error);
        return NULL;
    }

    conn->client_id = json_get_string(parsed_json, "client_id");
    conn->client_secret = json_get_string(parsed_json, "client_secret");
    conn->instance = strdup(url);
    json_object_put(parsed_json);
    if (!conn->client_id || !conn->client_secret) {
        fprintf(stderr, "LIBKENOMA_ERR: NULL conn fields found\n");
        return NULL;
    }

    // REQUESTING APP TOKEN
    // TODO: Should we check the return of this function?
    conn->app_token =
      kma_getToken(conn, scope, redirect_uri, NULL, KMA_APP_TOKEN);

    return conn;
}

kma_conn*
kma_recreateConn(char* client_id,
                 char* client_secret,
                 char* instance,
                 char* app_token,
                 char* user_token)
{
    kma_conn* conn = malloc(sizeof(kma_conn));
    if (!conn) {
        fputs("LIBKENOMA_ERR: Couldn't alloc memory for conn recreation",
              stderr);
        return NULL;
    }
    conn->client_id = strdup(client_id);
    conn->client_secret = strdup(client_secret);
    conn->instance = strdup(instance);
    conn->app_token = strdup(app_token);
    conn->user_token = strdup(user_token);
    return conn;
};

// as kma_conn is dinamically allocated, free is needed
void
kma_destroyConn(kma_conn* conn)
{
    if (conn) {
        if (conn->client_id)
            free(conn->client_id);
        if (conn->client_secret)
            free(conn->client_secret);
        if (conn->instance)
            free(conn->instance);
        if (conn->app_token)
            free(conn->app_token);
        if (conn->user_token)
            free(conn->user_token);
        free(conn);
    }
}

char*
kma_getAuthURL(kma_conn* conn,
               const char* scope,
               const char* redirect_uri,
               uint8_t force_login)
{
    if (!conn) {
        handle_error("Tried to get URL from NULL conn", KMA_ERR_NULL, 0);
        return NULL;
    }

    char* req_uri = NULL;
    // Mastodon API specifies that response_type should always be equal to
    // "code"
    asprintf(&req_uri,
             "%s%s?response_type=code&client_id=%s&redirect_uri=%s&scope=%s&"
             "force_login=%d",
             conn->instance,
             "/oauth/authorize",
             conn->client_id,
             redirect_uri,
             scope,
             force_login);

    if (!req_uri)
        handle_error("Couldn't alloc space/construct the user auth URL",
                     KMA_ERR_MEMORY,
                     0);

    return req_uri;
}

char*
kma_getToken(kma_conn* conn,
             const char* scope,
             const char* redirect_uri,
             const char* code,
             uint8_t type)
{
    if (!conn) {
        handle_error("Tried to get token from NULL conn", KMA_ERR_NULL, 0);
        return NULL;
    }

    char* req_uri = NULL;
    asprintf(&req_uri, "%s%s", conn->instance, "/oauth/token");

    char* grant_type = type ? "client_credentials" : "authorization_code";

    // IF GRANT_TYPE = authorization_code ITS FOR A USER.
    // IF ITS client_credentials ITS FOR A CLIENT (CONN)
    char* post_fields = NULL;
    asprintf(
      &post_fields,
      "grant_type=%s&client_id=%s&client_secret=%s&redirect_uri=%s&scope=%"
      "s&code=%s",
      grant_type,
      conn->client_id,
      conn->client_secret,
      redirect_uri,
      scope,
      code);

    char* ret_json = request_post(req_uri, post_fields);
    free(req_uri);
    free(post_fields);

    if (!ret_json) {
        handle_error("Could not get token, returning NULL", KMA_ERR_REQ, 0);
        return NULL;
    }

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    free(ret_json);
    char* token = json_get_string(parsed_json, "access_token");
    json_object_put(parsed_json);
    return token;
}

void
kma_getUserToken(kma_conn* conn,
                 const char* scope,
                 const char* redirect_uri,
                 const char* code)
{
    conn->user_token =
      kma_getToken(conn, scope, redirect_uri, code, KMA_USER_TOKEN);
}

int
verify_token(const kma_conn* conn, uint8_t type, kma_account** account)
{
    if (conn == NULL) {
        handle_error(
          "Tried to verify token from NULL connection.", KMA_ERR_NULL, 0);
        return 0;
    }

    char* type_url = type == KMA_APP_TOKEN ? "apps" : "accounts";

    char* verify_url = NULL;
    asprintf(
      &verify_url, "%s/api/v1/%s/verify_credentials", conn->instance, type_url);
    if (!verify_url) {
        handle_error("Couldn't allocate space for token verification URL",
                     KMA_ERR_MEMORY,
                     0);
        return 0;
    }

    char* token = type ? conn->app_token : conn->user_token;

    // GET Request
    char* ret_json = request_get_auth(verify_url, token);
    free(verify_url);
    if (!ret_json) {
        handle_error(
          "Could not request user information from token.", KMA_ERR_NULL, 0);
        return 0;
    }

    struct json_object* parsed_json = json_tokener_parse(ret_json);
    free(ret_json);

    // Don't even attempt to create user if error is returned. (saves some time)
    char* error = json_get_string(parsed_json, "error");
    if (error) {
        json_object_put(parsed_json);
        handle_error(
          "Could not get account information from user token, error returned:",
          KMA_ERR_REQ,
          0);
        fputs(error, stderr);
        return 0;
    }

    // Attempt to retrieve user infomation. If itsn't there, then return false
    if (account != NULL && type == KMA_USER_TOKEN) {
        *account = account_from_json(parsed_json);

        if (*account == NULL) {
            json_object_put(parsed_json);
            handle_error("Could not get account information from user token. "
                         "Returned object isn't an account nor an error.",
                         KMA_ERR_REQ,
                         0);
        }

        return 0;
    }

    json_object_put(parsed_json);

    return 1;
}

int
kma_verifyUser(const kma_conn* conn)
{
    return verify_token(conn, KMA_USER_TOKEN, NULL);
}

int
kma_verifyApp(const kma_conn* conn)
{
    return verify_token(conn, KMA_APP_TOKEN, NULL);
}
