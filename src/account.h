#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include "../include/kma_account.h"
#include "json.h"

kma_account*
account_from_json(struct json_object* json);

#endif // ACCOUNT_H_
