#include "../include/kma_timeline.h"

#include <curl/curl.h>
#include <json-c/json.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "request.h"
#include "status.h"
#include "utils.h"

kma_status**
kma_getTL(const kma_conn* conn,
          uint8_t local,
          uint8_t remote,
          uint8_t only_media,
          const char* max_id,
          const char* since_id,
          const char* min_id,
          int limit,
          uint8_t type // 0 = Public, 1 = Home
)
{
    if (conn == NULL || conn->instance == NULL) {
        handle_error(
          "Can't get timeline, NULL connection passed.", KMA_ERR_NULL, 0);
        return NULL;
    }

    // Assemble request URL
    /////////

    char* type_str = NULL;

    // Decide the type of TL and check the token requirements
    if (type == KMA_PUBLIC_TL && (conn->app_token || conn->user_token)) {
        type_str = "public";
    } else if (type == KMA_HOME_TL && conn->user_token) {
        type_str = "home";
    } else {
        handle_error(
          "Can't get timeline, insufficient authorization", KMA_ERR_NULL, 0);
        return NULL;
    }

    // Assemble base URL
    // local and remote can't be NULL and if they're needed they're ignored,
    // so, it doesn't hurt to put them there.
    //
    // NOTE: Mastodon documentation says local in the home timeline works, but
    //       on my tests it doesn't. I'm leaving it there, but it seems to have
    //       no purpose
    // TODO: Since the local flag has no effect, should it be used only under
    //       KMA_PUBLIC_TL? Test behaviour under Pleroma
    char* url = NULL;
    asprintf(&url,
             "%s/api/v1/timelines/%s?local=%d&remote=%d",
             conn->instance,
             type_str,
             local,
             remote);
    if (url == NULL) {
        handle_error(
          "Couldn't alloc space for timeline URL", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    // Add parameters
    // TODO: This "first" thing doesn't belong if there are parameter that are
    //       always gonna be there (the local flag).
    // TODO: Check for NULL in every asprintf
    uint8_t first = 0; // 1;
    if (limit > 0) {
        asprintf(&url, "%s%climit=%d", url, first ? '?' : '&', limit);
        first = 0;
    }
    if (max_id != NULL) {
        asprintf(&url, "%s%cmax_id=%s", url, first ? '?' : '&', max_id);
        first = 0;
    }
    if (since_id != NULL) {
        asprintf(&url, "%s%csince_id=%s", url, first ? '?' : '&', since_id);
        first = 0;
    }
    if (min_id != NULL) {
        asprintf(&url, "%s%cmin_id=%s", url, first ? '?' : '&', min_id);
        first = 0;
    }

    // Getting the JSON
    /////////

    // Request JSON
    char* ret_json = request_get_auth(url, conn->user_token ?: conn->app_token);
    if (ret_json == NULL) {
        handle_error("Couldn't get timeline JSON", KMA_ERR_OTHER, 0);
        printf("%s\n", url);
        return NULL;
    }
    free(url);
    // Parse JSON
    struct json_object* parsed_json = json_tokener_parse(ret_json);
    if (parsed_json == NULL) {
        handle_error("Error parsing timeline JSON", KMA_ERR_OTHER, 0);
        return NULL;
    }
    free(ret_json);

    // Constructing the array of posts
    /////////

    // Number of posts recieved (May be different from limit)
    int status_count = json_object_array_length(parsed_json);

    kma_status** status_array = NULL;
    // Allocating space for the array (+1 for delimiting NULL position)
    status_array = malloc((status_count + 1) * sizeof(kma_status*));
    if (status_array == NULL) {
        json_object_put(parsed_json);
        handle_error("Couldn't alloc space for timeline's status array",
                     KMA_ERR_MEMORY,
                     0);
        return NULL;
    }

    // Actually filling it
    for (int i = 0; i < status_count; i++) {
        status_array[i] =
          status_from_json(json_object_array_get_idx(parsed_json, i));
        if (status_array[i] == NULL) {
            free(status_array);
            json_object_put(parsed_json);
            handle_error("Failed to parse JSON array into posts, aborting.",
                         KMA_ERR_OTHER,
                         0);
            return NULL;
        }
    }
    // Null terminator
    status_array[status_count] = NULL;

    json_object_put(parsed_json);
    return status_array;
}

kma_status**
kma_getFederatedTL(const kma_conn* conn,
                   const char* max_id,
                   const char* since_id,
                   const char* min_id,
                   int limit)
{
    return kma_getTL(
      conn, 0, 1, 0, max_id, since_id, min_id, limit, KMA_PUBLIC_TL);
}

kma_status**
kma_getLocalTL(const kma_conn* conn,
               const char* max_id,
               const char* since_id,
               const char* min_id,
               int limit)
{
    return kma_getTL(
      conn, 1, 0, 0, max_id, since_id, min_id, limit, KMA_PUBLIC_TL);
}

kma_status**
kma_getHomeTL(const kma_conn* conn,
              const char* max_id,
              const char* since_id,
              const char* min_id,
              int limit)
{
    return kma_getTL(
      conn, 0, 0, 0, max_id, since_id, min_id, limit, KMA_HOME_TL);
}

void
kma_destroyTL(kma_status** statuses)
{
    if (statuses) {
        for (int i = 0; statuses[i] != NULL; i++) {
            kma_destroyStatus(statuses[i]);
        }
        free(statuses);
    }
}

kma_statusThread*
kma_getThreadStatuses(const kma_conn* conn, const char* id)
{
    if (conn == NULL || conn->instance == NULL) {
        handle_error(
          "Can't get timeline, NULL connection passed.", KMA_ERR_NULL, 0);
        return NULL;
    }

    // Assemble request URL
    /////////

    // Assemble base URL
    // local and remote can't be NULL and if they're needed they're ignored,
    // so, it doesn't hurt to put them there.
    //
    // TODO: Since the local flag has no effect, should it be used only under
    //       KMA_PUBLIC_TL? Test behaviour under Pleroma
    char* url = NULL;
    asprintf(&url, "%s/api/v1/statuses/%s/context/", conn->instance, id);
    if (url == NULL) {
        handle_error(
          "Couldn't alloc space for timeline URL", KMA_ERR_MEMORY, 0);
        return NULL;
    }

    // Getting the JSON
    /////////

    // Request JSON
    char* ret_json = request_get_auth(url, conn->user_token ?: conn->app_token);
    free(url);
    if (ret_json == NULL) {
        handle_error("Couldn't get timeline JSON", KMA_ERR_OTHER, 0);
        return NULL;
    }
    // Parse JSON
    struct json_object* parsed_json = json_tokener_parse(ret_json);
    if (parsed_json == NULL) {
        handle_error("Error parsing timeline JSON", KMA_ERR_OTHER, 0);
        return NULL;
    }
    free(ret_json);

    // Constructing the array of posts

    /* The resulting json is composed of two arrays of statuses, the ancestors,
     * and the descendants, in that order */

    // Ancestors

    struct json_object* ancestors_json =
      json_object_array_get_idx(parsed_json, 0);

    int ancestor_count = json_object_array_length(ancestors_json);

    kma_status** status_array_ancestors = NULL;
    // Allocating space for the array (+1 for delimiting NULL position)
    status_array_ancestors = malloc((ancestor_count + 1) * sizeof(kma_status*));
    if (status_array_ancestors == NULL) {
        json_object_put(parsed_json);
        handle_error("Couldn't alloc space for timeline's status array",
                     KMA_ERR_MEMORY,
                     0);
        return NULL;
    }

    // Actually filling it
    for (int i = 0; i < ancestor_count; i++) {
        status_array_ancestors[i] =
          status_from_json(json_object_array_get_idx(ancestors_json, i));
        if (status_array_ancestors[i] == NULL) {
            free(status_array_ancestors);
            json_object_put(ancestors_json);
            handle_error("Failed to parse JSON array into posts, aborting.",
                         KMA_ERR_OTHER,
                         0);
            return NULL;
        }
    }
    json_object_put(ancestors_json);
    // Null terminator
    status_array_ancestors[ancestor_count] = NULL;

    // Descendants

    struct json_object* descendants_json =
      json_object_array_get_idx(parsed_json, 1);

    json_object_put(parsed_json);

    int descendant_count = json_object_array_length(descendants_json);

    kma_status** status_array_descendants = NULL;
    // Allocating space for the array (+1 for delimiting NULL position)
    status_array_descendants =
      malloc((descendant_count + 1) * sizeof(kma_status*));
    if (status_array_descendants == NULL) {
        json_object_put(descendants_json);
        handle_error("Couldn't alloc space for timeline's status array",
                     KMA_ERR_MEMORY,
                     0);
        return NULL;
    }

    // Actually filling it
    for (int i = 0; i < descendant_count; i++) {
        status_array_descendants[i] =
          status_from_json(json_object_array_get_idx(descendants_json, i));
        if (status_array_descendants[i] == NULL) {
            free(status_array_descendants);
            json_object_put(descendants_json);
            handle_error("Failed to parse JSON array into posts, aborting.",
                         KMA_ERR_OTHER,
                         0);
            return NULL;
        }
    }
    json_object_put(descendants_json);
    // Null terminator
    status_array_descendants[descendant_count] = NULL;

    // Assembling the arrays into a struct to return it

    kma_statusThread* thread = malloc(sizeof(kma_statusThread));
    thread->ancestors = status_array_ancestors;
    thread->self = kma_getStatus(conn, id);
    thread->descendants = status_array_descendants;

    return thread;
}

void
kma_destroyStatusThread(kma_statusThread* thread)
{
    if (thread != NULL) {
        if (thread->ancestors)
            kma_destroyTL(thread->ancestors);
        if (thread->self)
            kma_destroyStatus(thread->self);
        if (thread->descendants)
            kma_destroyTL(thread->ancestors);
        free(thread);
    }
}
