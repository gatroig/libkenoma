#ifndef STATUS_H_
#define STATUS_H_

#include <json-c/json.h>

#include "../include/kma_status.h"

kma_status*
status_from_json(struct json_object* json);

#endif // STATUS_H_
