#include "kma_auth.h"

#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>

kma_conn conn = { "client_id",
                  "client_secret",
                  "instance",
                  "app_token",
                  "user_token" };
char* conn_json = "{"
                  "\"client_id\"     : \".\","
                  "\"client_secret\" : \".\","
                  "\"instance\"      : \".\","
                  "\"app_token\"     : \".\","
                  "\"user_token\"    : \".\""
                  "}";

void*
__wrap_malloc(size_t size)
{
    (void)size;
    return mock_type(void*);
}

int
__wrap_asprintf(char** dest, void* rest)
{
    *dest = mock_type(char*);

    if (*dest == NULL) {
        return -1;
    } else {
        return strlen(*dest);
    }
}

void
__wrap_free(void* ptr)
{
    (void)ptr;
}

char*
__wrap_request_post(void)
{
    return mock_type(char*);
}

static void
create_conn_invalid_params()
{
    assert_null(kma_createConn(NULL, "", "", "", ""));
    assert_null(kma_createConn("", NULL, "", "", ""));
    assert_null(kma_createConn("", "", NULL, "", ""));
    assert_null(kma_createConn("", "", "", NULL, ""));
    assert_null(kma_createConn("", "", "", "", NULL));

    // It isn't the create conn's job to test if the domain is valid
    /*assert_null(kma_createConn("", "", "", "", ""));
    assert_null(kma_createConn("invalid.tld", "", "", "", ""));
    assert_null(kma_createConn("example.com", "", "", "", ""));*/
}

static void
create_conn_alloc_fail()
{
    // Allocating the new conn struct fails
    will_return(__wrap_malloc, NULL);
    assert_null(kma_createConn("fedi.fai.st", "", "", "", ""));

    // Allocating space for the URL fails
    will_return(__wrap_malloc, &conn);
    will_return(__wrap_asprintf, NULL);
    assert_null(kma_createConn("fedi.fai.st", "", "", "", ""));

    // Allocation for the request POST parameters
    will_return(__wrap_malloc, &conn);
    will_return(__wrap_asprintf, "instance.tld/api/v1/apps");
    will_return(__wrap_asprintf, NULL);
    assert_null(kma_createConn("fedi.fai.st", "", "", "", ""));
}

static void
create_conn_request_error()
{
    // Request failed with a NULL return
    will_return(__wrap_malloc, &conn);
    will_return(__wrap_asprintf, "instance.tld/api/v1/apps");
    will_return(__wrap_asprintf, "POST params");
    will_return(__wrap_request_post, conn_json);
    assert_null(kma_createConn("fedi.fai.st", "", "", "", ""));
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(create_conn_invalid_params),
        cmocka_unit_test(create_conn_alloc_fail),
        cmocka_unit_test(create_conn_request_error),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
